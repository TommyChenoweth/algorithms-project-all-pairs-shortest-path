My project will compile into two executables: a graph generator named "gg", and an all pairs shortest path solver named "apsp".

=======================================================
Compiling
=======================================================
I've provided a makefile to simplify compilation. To compile my project, simply use the make command while in the same directory as this readme.

=======================================================
Graph Generator Usage
=======================================================
The graph generator accepts a number of command line arguments that I'll list here:
-G=<val>        Set the number of graphs to generate.
-N=<val>        Set the number of vertices to generate for each graph.
-O=<filename>   Set the output filename.
-R=<low>-<high> Set the range to use for random weight assignment.
-T=<f|r>        Set the type of weight generator to use: r random or f for fixed.
-W=<val>        Set the weight to use for fixed weight assignment.

For example, the generate a file named graphs.txt containing 10 graphs with random weight assignments ranging from 1 to 100, you would use the following arguments:
./gg -G=10 -N=50 -T=r -R=1-100

If you don't specify a filename, the graph generator will name the output file graphs.txt.

If you don't specify any command line arguments, the graph generator will output a file graphs.txt containing one graph with 50 vertices whose edge weights are randomly generated and in the range [1, 100].

=======================================================
All Pairs Shortest Path Usage
=======================================================
The all pairs shortest path solver accepts only one command line argument: the input filename. If no input filename is specified, the solver tries to use graphs.txt.

For example, to solve all pairs shortest path for any number of graphs in graphs.txt, you would use the following command:
./apsp

To solve the all pairs shortest path for any number of graphs in test.txt, you would use the following command:
./apsp test.txt

The all pairs shortest path solver will accept any number of graphs in a single file, and send each graph through both Dijkstra's and Floyd-Warshall's algorithms while calculating the average running time. The solver will output the average running time and resulting all pairs shortest path matrices of dijkstra's to a file named graphs_apsp_dijkstra.txt. It will output the average running time and resulting all pairs shortest path matrices of Floyd-Warshall's to a file named graphs_apsp_floyd_warshall.txt.

=======================================================
All Pairs Shortest Path Input Format
=======================================================
The all pairs shortest path solver accepts as input a file with any number of cost adjacency matrices separated by empty lines. A file containing two graphs with 4 vertices might look like this:

0 100 4 8 
1 0 24 83 
71 33 0 11 
48 91 65 0 

0 21 45 89 
76 0 8 29 
44 14 0 99 
90 26 5 0 

Note the trailing space on each row of the matrix; that is important because the all pairs shortest path solver uses the number of spaces in the first line to determine the dimensions of the graph.

=======================================================
Misc
=======================================================
In the bin folder, I've included the scripts that I used to test the running time of the all pairs shorstest path algorithms:

fixed_go.sh
This script will generate two fixed weight graphs for each number of vertices in the series {50, 80, 110, ..., 1010}, send the resulting graphs though the all pairs shortest path solver, and then copy all of the input and output files to a directories named by the number of vertices in the graph. 

random_go.sh
This script does the same thing as fixed_go.sh except it generates 10 graphs for each number of vertices, and uses random weight assignment.