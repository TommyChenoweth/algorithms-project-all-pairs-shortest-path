#pragma once

#include <iostream>
#include <vector>

class cost_adjacency_matrix
{
private:
	int _number_of_vertices;
	std::vector<std::vector<int>> _cost_adjacency_matrix;

public:
	cost_adjacency_matrix(const int number_of_vertices);

	int get_number_of_vertices() const;
	void set_number_of_vertices(const int number_of_vertices);

	int& operator()(const int row, const int column);
	int operator()(const int row, const int column) const;
	std::vector<int>& operator()(const int row);
	std::vector<int> operator()(const int row) const;
	friend std::ostream& operator<<(std::ostream& os, const cost_adjacency_matrix& cam);
	friend std::istream& operator>>(std::istream& is, cost_adjacency_matrix& cam);
};