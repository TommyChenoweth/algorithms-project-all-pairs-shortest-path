#include "cost_adjacency_matrix.hpp"

#include <algorithm>
#include <cassert>
#include <string>

cost_adjacency_matrix::cost_adjacency_matrix(const int number_of_vertices)
	: _number_of_vertices(number_of_vertices)
{
	set_number_of_vertices(_number_of_vertices);
}

int cost_adjacency_matrix::get_number_of_vertices() const
{
	return _number_of_vertices;
}

void cost_adjacency_matrix::set_number_of_vertices(const int number_of_vertices)
{
	_number_of_vertices = number_of_vertices;

	_cost_adjacency_matrix.resize(_number_of_vertices);
	for(auto& row : _cost_adjacency_matrix)
	{
		row.resize(_number_of_vertices, 0);
	}
}

int& cost_adjacency_matrix::operator()(const int row, const int column)
{
	assert(-1 < row && row < _number_of_vertices);
	assert(-1 < column && column < _number_of_vertices);

	return _cost_adjacency_matrix[row][column];
}

int cost_adjacency_matrix::operator()(const int row, const int column) const
{
	assert(-1 < row && row < _number_of_vertices);
	assert(-1 < column && column < _number_of_vertices);

	return _cost_adjacency_matrix[row][column];
}

std::vector<int>& cost_adjacency_matrix::operator()(const int row)
{
	return _cost_adjacency_matrix[row];
}

std::vector<int> cost_adjacency_matrix::operator()(const int row) const
{
	return _cost_adjacency_matrix[row];
}

std::ostream& operator<<(std::ostream& os, const cost_adjacency_matrix& cam)
{
	for (int row = 0; row < cam._number_of_vertices; ++row)
	{
		for (int column = 0; column < cam._number_of_vertices; ++column)
		{
			os << cam._cost_adjacency_matrix[row][column] << " ";
		}
		os << std::endl;
	}
	return os;
}

std::istream& operator>>(std::istream& is, cost_adjacency_matrix& cam)
{
	std::vector<char> buffer(256);

	int number_of_vertices = 0;

	bool know_new_number_of_vertices(false);
	while(!know_new_number_of_vertices)
	{
		is.getline(&buffer[0], buffer.size());

		if(is.fail())
		{
			// The input buffer is too small; we'll resize it.
			buffer.resize(buffer.size() * 2);
			is.clear();

			// Move the get pointer back to where it was before the read.
			is.seekg(-is.gcount(), is.cur);

			continue;
		}

		number_of_vertices = std::count(buffer.begin(), buffer.begin() + is.gcount(), ' ');
		know_new_number_of_vertices = true;

		// Move the get pointer back to where it was before the read.
		is.seekg(-is.gcount(), is.cur);
	}

	// Resize the cost adjacency matrix to make room for the new number of vertices.
	cam.set_number_of_vertices(number_of_vertices);

	// Fill the cost adjacency matrix with the values from the stream.
	auto row_it = cam._cost_adjacency_matrix.begin();
	while(row_it != cam._cost_adjacency_matrix.end())
	{
		for(auto& edge : *row_it)
		{
			is >> edge;
		}

		++row_it;
	}

	const std::string whitespace_characters = {' ', '\n', '\r'};

	// Skip past whitespace characters.
	bool next_char_is_whitespace(true);
	while(next_char_is_whitespace)
	{
		const char next = is.peek();
		if(std::string::npos != whitespace_characters.find(next))
			is.seekg(1, is.cur);
		else
			next_char_is_whitespace = false;
	}

	return is;
}