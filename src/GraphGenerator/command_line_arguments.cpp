#include "command_line_arguments.hpp"

void set_number_of_graphs(const char* s, user_values& v);
void set_number_of_vertices(const char* s, user_values& v);
void set_weight_generator_type(const char* s, user_values& v);
void set_output_filename(const char* s, user_values& v);
void set_fixed_weight_value(const char* s, user_values& v);
void set_random_range(const char* s, user_values& v);

std::map<char, command_line_argument> paramkey_to_description =
{
	{'G', {"-G=<val>", "Set the number of graphs to generate.", set_number_of_graphs}},
	{'N', {"-N=<val>", "Set the number of vertices to generate for each graph.", set_number_of_vertices}},
	{'T', {"-T=<f|r>", "Set the type of weight generator to use.", set_weight_generator_type}},
	{'O', {"-O=<filename>", "Set the output filename.", set_output_filename}},
	{'W', {"-W=<val>", "Set the weight to use for fixed weight assignment.", set_fixed_weight_value}},
	{'R', {"-R=<low>-<high>", "Set the range to use for random weight assignment.", set_random_range}}
};

void set_number_of_graphs(const char* s, user_values& v)
{
	v.number_of_graphs = atoi(&s[3]);
}

void set_number_of_vertices(const char* s, user_values& v)
{
	v.number_of_vertices = atoi(&s[3]);
}

void set_weight_generator_type(const char* s, user_values& v)
{
	switch(s[3])
	{
	case 'r':
		v.gen_type = graph_generator_type::random_weight;
		break;

	case 'f':
		v.gen_type = graph_generator_type::fixed_weight;
		break;

	default:
		break;
	}
}

void set_output_filename(const char* s, user_values& v)
{
	v.output_filename = std::string(&s[3]);
}

void set_fixed_weight_value(const char* s, user_values& v)
{
	v.weight = atoi(&s[3]);
}

void set_random_range(const char* s, user_values& v)
{
	// Skip the first hyphen.
	auto delimiter_position = std::string(s+1).find('-');
	if(delimiter_position == std::string::npos)
		return;

	v.low = atoi(&s[3]);
	v.high = atoi(&s[delimiter_position + 2]);
}