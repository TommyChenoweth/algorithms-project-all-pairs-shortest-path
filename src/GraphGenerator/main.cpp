#include <ctime>
#include <fstream>
#include <functional>
#include <iostream>
#include <random>
#include <string>
#include <vector>

#include "command_line_arguments.hpp"
#include "cost_adjacency_matrix.hpp"

std::default_random_engine g_random_engine(time(0));
std::uniform_int_distribution<int> g_uniform_dist(1, 100);

void assign_weights_with_function(cost_adjacency_matrix& cam, std::function<int()> weight_gen);

cost_adjacency_matrix generate_complete_graph_fixed_weight(const int number_of_vertices, const int weight);
cost_adjacency_matrix generate_complete_graph_random_weight(const int number_of_vertices);

int main(int argc, const char* argv[])
{
	if(argc < 2)
	{
		for(const auto& kvp : paramkey_to_description)
		{
			std::cout << kvp.second.usage << "\t" << kvp.second.description << std::endl;
		}
		return 0;
	}

	// Default parameters
	user_values v;
	v.number_of_graphs = 1;
	v.number_of_vertices = 50;
	v.gen_type = graph_generator_type::random_weight;
	v.output_filename = "graphs.txt";
	v.weight = 10;
	v.low = 1;
	v.high = 100;

	// Parse command line parameters
	for(int i = 1; i < argc; ++i)
	{
		paramkey_to_description[argv[i][1]].evaluate(argv[i], v);
	}

	std::vector<cost_adjacency_matrix> graphs;

	// Generate the graphs
	switch(v.gen_type)
	{
	case graph_generator_type::fixed_weight:
		for(int i = 0; i < v.number_of_graphs; ++i)
			graphs.push_back(generate_complete_graph_fixed_weight(v.number_of_vertices, v.weight));
		break;

	case graph_generator_type::random_weight:
		for(int i = 0; i < v.number_of_graphs; ++i)
			graphs.push_back(generate_complete_graph_random_weight(v.number_of_vertices));
		break;

	default:
		break;
	}

	// Output the generated graphs
	std::ofstream file(v.output_filename.c_str());
	for(const cost_adjacency_matrix& cam : graphs)
	{
		file << cam << std::endl;
	}

	return 0;
}

void assign_weights_with_function(cost_adjacency_matrix& cam, std::function<int()> weight_gen)
{
	auto number_of_vertices = cam.get_number_of_vertices();
	for(int row = 0; row < number_of_vertices; ++row)
	{
		for(int column = 0; column < number_of_vertices; ++column)
		{
			// A vertex shouldn't have an edge to itself.
			if(row == column)
				continue;

			cam(row, column) = weight_gen();
		}
	}
}

cost_adjacency_matrix generate_complete_graph_fixed_weight(const int number_of_vertices, const int weight)
{
	cost_adjacency_matrix cam(number_of_vertices);

	assign_weights_with_function(cam, [weight](){return weight; });

	return cam;
}

cost_adjacency_matrix generate_complete_graph_random_weight(const int number_of_vertices)
{
	cost_adjacency_matrix cam(number_of_vertices);

	assign_weights_with_function(cam, [](){return g_uniform_dist(g_random_engine); });

	return cam;
}