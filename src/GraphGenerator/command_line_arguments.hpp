#pragma once

#include <functional>
#include <map>
#include <string>

enum class graph_generator_type
{
	fixed_weight,
	random_weight
};

struct user_values
{
	int number_of_graphs;
	int number_of_vertices;
	graph_generator_type gen_type;
	std::string output_filename;
	int weight;
	int low;
	int high;
};

struct command_line_argument
{
	command_line_argument(const char* usage, const char* description, std::function<void(const char*, user_values&)> evaluate)
	: usage(usage),
	  description(description),
	  evaluate(evaluate)
	{
	}

	command_line_argument()
	: usage("ERROR"),
	  description("ERROR"),
	  evaluate([](const char* s, user_values& v){})
	{
	}

	std::string usage;
	std::string description;
	std::function<void(const char*, user_values&)> evaluate;
};

extern std::map<char, command_line_argument> paramkey_to_description;