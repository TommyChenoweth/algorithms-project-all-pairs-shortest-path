#include <algorithm>
#include <climits>
#include <ctime>
#include <fstream>
#include <iostream>
#include <string>

#include "../GraphGenerator/cost_adjacency_matrix.hpp"

std::vector<int> dijkstra(const cost_adjacency_matrix& cam, const int v, double& out_runtime);
cost_adjacency_matrix apsp_dijkstra(const cost_adjacency_matrix& cam, double& out_runtime);

cost_adjacency_matrix apsp_floyd_warshall(const cost_adjacency_matrix& cam, double& out_runtime);

int main(int argc, char* argv[])
{
	std::vector<cost_adjacency_matrix> graphs;

	std::cout << "Reading graphs";

	std::string input_filename("graphs.txt");
	if(1 < argc)
		input_filename = std::string(argv[1]);

	// Read in cost adjacency matrices from a file.
	std::ifstream graphs_file(input_filename, std::ios_base::binary);
	while(graphs_file.is_open() && !graphs_file.eof())
	{
		cost_adjacency_matrix cam(1);
		graphs_file >> cam;
		graphs.push_back(cam);

		std::cout << ".";
	}
	graphs_file.close();

	std::cout << std::endl;

	std::vector<cost_adjacency_matrix> graphs_apsp_dijkstra;
	std::vector<cost_adjacency_matrix> graphs_apsp_floyd_warshall;

	double runtime_dijkstra_total(0.0);
	double runtime_floyd_warshall_total(0.0);

	std::cout << "Computing APSP";

	// Send the cost adjacency matrices through the APSP algorithms.
	for(const auto& graph : graphs)
	{
		std::cout << ".";

		double runtime_dijkstra(0.0);
		graphs_apsp_dijkstra.push_back(apsp_dijkstra(graph, runtime_dijkstra));
		runtime_dijkstra_total += runtime_dijkstra;

		double runtime_floyd_warshall(0.0);
		graphs_apsp_floyd_warshall.push_back(apsp_floyd_warshall(graph, runtime_floyd_warshall));
		runtime_floyd_warshall_total += runtime_floyd_warshall;
	}
	std::cout << std::endl;

	double runtime_dijkstra_average = runtime_dijkstra_total / graphs.size();
	double runtime_floyd_warshall_average = runtime_floyd_warshall_total / graphs.size();

	// Write the APSP matrices to separate files so that they can be easily
	// compared with windiff.
	std::ofstream apsp_dijkstra_file("graphs_apsp_dijkstra.txt");
	apsp_dijkstra_file << "Average Runtime: " << runtime_dijkstra_average << std::endl << std::endl;
	for(const auto& apsp : graphs_apsp_dijkstra)
	{
		apsp_dijkstra_file << apsp << std::endl;
	}
	apsp_dijkstra_file.close();

	std::ofstream apsp_floyd_warshall_file("graphs_apsp_floyd_warshall.txt");
	apsp_floyd_warshall_file << "Average Runtime: " << runtime_floyd_warshall_average << std::endl << std::endl;
	for(const auto& apsp : graphs_apsp_floyd_warshall)
	{
		apsp_floyd_warshall_file << apsp << std::endl;
	}
	apsp_floyd_warshall_file.close();

	return 0;
}

std::vector<int> dijkstra(const cost_adjacency_matrix& cam, const int v, double& out_runtime)
{
	const int number_of_vertices = cam.get_number_of_vertices();

	auto ticks_begin = clock();

	// The variable names and comments in this function are taken from the pseudocode.

	std::vector<bool> s(number_of_vertices, false);
	s[v] = true;

	std::vector<int> dist = cam(v);

	// When this loop terminates, N-1 vertices should be in s. Because
	// we added v to s during initialization above, we only need to 
	// perform N-2 iterations.
	for(int i = 0; i < number_of_vertices - 2; ++i)
	{
		// Choose u from among those vertices not in s such that
		// dist[u] is minimum.
		int smallest_distance = INT_MAX;
		int u = -1;
		for(int j = 0; j < number_of_vertices; ++j)
		{
			if(!s[j] && dist[j] < smallest_distance)
			{
				smallest_distance = dist[j];
				u = j;
			}
		}

		s[u] = true;

		// Update distances.
		for(int w = 0; w < number_of_vertices; ++w)
		{
			if(!s[w] && dist[w] > dist[u] + cam(u, w))
				dist[w] = dist[u] + cam(u, w);
		}
	}

	auto ticks_end = clock();
	out_runtime = static_cast<double>(ticks_end - ticks_begin) / CLOCKS_PER_SEC;

	return dist;
}

cost_adjacency_matrix apsp_dijkstra(const cost_adjacency_matrix& cam, double& out_runtime)
{
	cost_adjacency_matrix apsp(cam.get_number_of_vertices());

	for(int i = 0; i < cam.get_number_of_vertices(); ++i)
	{
		double row_runtime(0.0);
		apsp(i) = dijkstra(cam, i, row_runtime);
		out_runtime += row_runtime;
	}

	return apsp;
}

cost_adjacency_matrix apsp_floyd_warshall(const cost_adjacency_matrix& cam, double& out_runtime)
{
	const int number_of_vertices = cam.get_number_of_vertices();

	auto ticks_begin = clock();

	// The variable names in this function are taken from the pseudocode.

	cost_adjacency_matrix A(cam);

	for(int k = 0; k < number_of_vertices; ++k)
	{
		for(int i = 0; i < number_of_vertices; ++i)
		{
			for(int j = 0; j < number_of_vertices; ++j)
			{
				A(i, j) = std::min(A(i, j), A(i, k) + A(k, j));
			}
		}
	}

	auto ticks_end = clock();
	out_runtime = static_cast<double>(ticks_end - ticks_begin) / CLOCKS_PER_SEC;

	return A;
}