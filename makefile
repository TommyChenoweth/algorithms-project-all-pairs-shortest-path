OUTPUT_DIRECTORY=./bin/
CFLAGS=-std=c++0x -O3

GRAPHGEN_EXE_NAME=gg
GRAPHGEN_SOURCE_DIR=./src/GraphGenerator
_GRAPHGEN_SOURCES=main.cpp cost_adjacency_matrix.cpp command_line_arguments.cpp
GRAPHGEN_SOURCES=$(patsubst %,$(GRAPHGEN_SOURCE_DIR)/%,$(_GRAPHGEN_SOURCES))

APSP_EXE_NAME=apsp
APSP_SOURCES=./src/AllPairsShortestPath/main.cpp ./src/GraphGenerator/cost_adjacency_matrix.cpp

all: directories graphgenerator allpairsshortestpath

directories: ; mkdir -p $(OUTPUT_DIRECTORY)

graphgenerator: ; g++ $(CFLAGS) $(GRAPHGEN_SOURCES) -o $(OUTPUT_DIRECTORY)$(GRAPHGEN_EXE_NAME)

allpairsshortestpath: ; g++ $(CFLAGS) $(APSP_SOURCES) -o $(OUTPUT_DIRECTORY)$(APSP_EXE_NAME)